import glob
import re

from helpers import  get_gbest_fitness, functions
from plotting import alpha_graph

COLORS = ("#00067A",
          "#17449B",
          "#2E82BC",
          "#45C0DD",
          "#5CEFFE",
          "#42EE20",
          "#38BE1A",
          "#2E8F14",
          "#245F0E",
          "#1A3008",
          "#000000")


def setup_graph(graph_files):
    exp = "out\\\\(.*?)-(.*?)-.*?(\d\.\d)"
    match = re.search(exp, graph_files[0])
    alpha_count = len(graph_files)
    prob = match.group(1)
    guide = match.group(2)
    colors = list(COLORS[::])
    print("Setting up", prob, guide)
    return {'problem': prob , 'guide': guide + "-global", 'data': [
        {'file': name,
         'alpha': re.search(exp, name).group(3),
         'data': get_gbest_fitness(name),
         'color': colors.pop()
         } for name in graph_files]}


def op(files):
    graph = setup_graph(files)
    print("Creating", graph['problem'])
    alpha_graph(graph, 1000, 200, word="Global Best")


patterns = ["-SocialRandom-Constant*.pickle",
            "-SocialSwarmBest-Constant*.pickle"]


if __name__ == '__main__':
    graphs_files = [glob.glob("out\{}".format(f + p)) for p in patterns for f in functions]
    from multiprocessing.pool import Pool

    with Pool() as pool:
        result = pool.map(op, graphs_files)
