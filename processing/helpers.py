import pickle
import re
import statistics

separator = ", "
newline = "\n"
problem_count = 6
runs = 50


def build_row(parameter, row):
    return [item[parameter] for item in row]


def extract_feature(parameter, data, row_builder=build_row):
    # swap rows & cols
    return [row_builder(parameter, row) for row in data]


def row_string(row):
    return separator.join(map(str, row))


def format_dictionary(dict, level=0):
    return newline.join([format_key(key, dict, level) for key in dict])


def format_key(key, obj, level):
    value = obj[key]
    format_string = "\t" * level + "{}:{}{}"
    if isinstance(value, dict):
        return format_string.format(key, newline, format_dictionary(value, level + 1))
    else:
        return format_string.format(key, " ", value)


def data_string(headings, feature_data):
    header = separator.join(headings)
    output = newline.join(row_string(row) for row in feature_data)
    return header + newline + output


def unpickle(in_file_name):
    return pickle.load(open(in_file_name, "rb"))


def stats_headings():
    return ["Min", "Max", "Med", "Average", "Std Dev", "Variance"]


def row_stats(row):
    return [min(row), max(row), statistics.mean(row), statistics.median(row),
            statistics.stdev(row), statistics.variance(row)]


def stats_data(data):
    return [row_stats(row) for row in data]


def folder_name(in_file_name, count=None):
    rfind = in_file_name.rfind(".")
    folder = (in_file_name[:rfind] if rfind > -1 else in_file_name) + "/"
    if count:
        folder = folder.replace("-", "/", count)
    else:
        folder = folder.replace("-", "/")
    return folder


def iteration_data(data_object, data_key='run-data'):
    return list(zip(*(data_object[data_key])))


def get_average_fitness(file_name):
    it_data = iteration_data(unpickle(file_name))
    return extract_feature("avg_fitness", it_data, avg_row)

def get_gbest_fitness(file_name):
    it_data = iteration_data(unpickle(file_name))
    return extract_feature("gbest", it_data, avg_row)

def avg_row(parameter, row):
    return statistics.mean(build_row(parameter, row))


def prob_name(file_name):
    exp = "out/(.*?)-.*?"
    return re.search(exp, file_name).group(1)


def problem_average_fitness(file_names):
    return {prob_name(name): get_average_fitness(name) for name in file_names}


def read_names(listfile_name):
    with open(listfile_name, "r") as file:
        return [l.strip() for l in file.readlines()]


functions = ["Absolute", "Ackley", "Griewank", "Rastrigin", "Salomon", "Spherical"]