import sys

from math import sqrt

from helpers import read_names, extract_feature, iteration_data, unpickle, problem_count, prob_name, runs

from scipy.stats import mannwhitneyu
# perform mann u whitney test on runs vs conventional guide
# popoulation is average swarm fitness from each run at the last iteration


def last_it_fitness(name):
    print('Getting last fitness', name)
    return name, extract_feature("avg_fitness", iteration_data(unpickle(name)))[-1]


if __name__ == '__main__':
    from multiprocessing.pool import Pool

    names = read_names(sys.argv[1])

    #conv_names = names[:problem_count]
    #problem_indices = {prob_name(conv_names[i]): i for i in range(len(conv_names))}
    other_names = names[problem_count:]

    with Pool() as pool:
        data = pool.map(last_it_fitness, names)

    conventional_data = {prob_name(names[i]): data[i][1] for i in range(problem_count)}

    n1 = n2 = runs
    mu = n1*n2/2.0
    devu = sqrt(n1*n2*(n1+n2+1)/12.0)

    def get_z(u):
        return (u-mu)/devu

    def do_manU(d):
        name = d[0]
        x1 = d[1]

        prob = prob_name(name)
        x2 = conventional_data[prob]

        muw = mannwhitneyu(x1, x2)
        u = muw[0]
        p = muw[1] * 2
        z = get_z(u)

        return prob, name, u, p, z  # two tailed hypothesis
    # A small p-value (≤ 0.05) indicates strong evidence against the null hypothesis, so it is rejected
    headings = ("Problem", "Filename", "u", "p", "z")
    results = [do_manU(d) for d in data[problem_count:]]


    with open(sys.argv[2], "w") as file:
        print("File name:", sys.argv[2])
        file.write("A small p value (<= 0.05) indicates strong evidence against the null hypothesis, so it is rejected\n")
        file.write(",".join(headings) + "\n")
        for line in results:
            file.write(",".join(map(str, line)) + "\n")
