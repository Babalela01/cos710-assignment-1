import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='Arial')
# data = [{name: ..., data: ..., settings: ...}]
def iterations_graph(iterations, data, title, file, cutoff, normal_data=None):
    plt.clf()
    x = range(iterations)[:cutoff]
    for series in data:
        y = series['data'][:cutoff]
        settings = series['settings']
        label = series['name']
        plt.plot(x, y, settings, label=label, color='#1E90FF')
    if normal_data:
        y = normal_data[:cutoff]
        settings = "-"
        color="black"
        label="Conventional Guide"
        plt.plot(x, y, settings, label=label, color=color)
    plt.xlabel('Time (iterations)')
    plt.ylabel('Fitness')
    plt.title(title)
    plt.legend()
    plt.savefig(file)
    plt.clf()


def alpha_graph(data, iterations, cutoff, alpha="\u03B1", word="Average"):
    plt.clf()
    x = range(iterations)[:cutoff]
    for series in data['data']:
        y = series['data'][:cutoff]
        settings = "-"
        label = "{} = {}".format(alpha, series['alpha'])
        color = series['color']
        plt.plot(x, y, settings, label=label, color=color)
    plt.xlabel('Time (iterations)')
    plt.ylabel('Fitness')
    problem_name = data['problem']

    plt.title(problem_name + ": " + word + " fitness for different " + alpha)
    plt.legend()
    file_name = 'images/' + problem_name + "/" + problem_name + "-" + data['guide'] + "-alphacomparison.png"
    plt.savefig(file_name)
    plt.clf()

