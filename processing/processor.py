import os
import sys

from helpers import *
from plotting import iterations_graph

# {
#     "boundary": ...,
#     "dimensions": ...,
#     "max-iterations": ...,
#     "problem": ...,
#     "velocity": ...,
#     "comparator": ...,
#     "swarm-size": ...,
#     "run-count": ...,
#     "name": ...,
#     "run-data": [
#        [ {"gbest": ..., "avg_pbest": ..., "avg_fitness": ..., "swarm_best": ...}, ... ]
#        [ {"gbest": ..., "avg_pbest": ..., "avg_fitness": ..., "swarm_best": ...}, ... ]
#        ....
#      ]
# }


extension = ".csv"
features = ["gbest", "avg_pbest", "avg_fitness", "swarm_best"]
graph_maxit = 300

settings_list = ["name", "problem", "boundary", "dimensions", "swarm-size", "velocity", "run-count", "comparator"]
run_headings = lambda run_count: ["Run #{}".format(i + 1) for i in range(run_count)]


def default_output(data_iterations, run_count, folder):
    run_heading = run_headings(run_count)
    for feature in features:
        feature_data = extract_feature(feature, data_iterations)

        #outfile = open(folder + feature + extension, "w")
        #outfile.write(data_string(run_heading, feature_data))
        #outfile.close()

        outfile = open(folder + "stats-" + feature + extension, "w")
        outfile.write(data_string(stats_headings(), stats_data(feature_data)))
        outfile.close()


def main(in_file_name):
    print("Handling", in_file_name)
    prob = prob_name(in_file_name)
    data_object = unpickle(in_file_name)
    data_iterations = iteration_data(data_object)  # swap rows columns to have per iteration instead of per run
    settings = {key: data_object[key] for key in settings_list}
    folder = folder_name(in_file_name)
    os.makedirs(folder, exist_ok=True)

    # run_count = settings['run-count']
    #picfolder = "images/" + folder_name(in_file_name, 1)[:-1].replace("out/", "")
    #os.makedirs(picfolder, exist_ok=True)

    default_output(data_iterations, 50, folder)
    # series = [{name: ..., data: ..., settings: ...}]
    normal_data = conv_data[prob]
    # best_graphs(data_iterations, (settings['problem'] + " function" + "\nAverages over 50 runs"), picfolder
    #           + "-best.png", normal_data)

    # outfile = open(folder + "settings.txt", "w")
    # outfile.write(format_dictionary(settings))
    # outfile.close()
    return True


def best_graphs(data_iterations, title, file_, normal_data):
    features = [("gbest", "Global best", "--"), ("avg_pbest", "Average Personal best", ":"),
                ("avg_fitness", "Average Fitness", '-')]
    graph = [{"name": t[1],
              "data": [statistics.mean(build_row(t[0], r))
                       for r in data_iterations],
              "settings": t[2]} for t in features]
    iterations_graph(len(data_iterations), graph, title, file_, graph_maxit, normal_data)


names = read_names(sys.argv[1])


conv_names = names[:problem_count]
conv_data = problem_average_fitness(conv_names)


if __name__ == '__main__':
    print("Starting")
    from multiprocessing.pool import Pool
    with Pool() as pool:
        print('Pool')
        result = pool.map(main, names)
