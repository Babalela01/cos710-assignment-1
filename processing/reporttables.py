from itertools import product

from helpers import functions

problems = functions
guide = ["SocialRandom", "SocialSwarmBest"]
strat = ["RanDimensions", "RanIteration", "Reduce"]

stats = ["gbest", "avg_fitness"]

guide_strat = list(product(guide, strat))
# prob/guide/strat/stat-{}.csv

normal_path = "out/{}/NormalSocial/stats-{}.csv"
path = "out/{}/{}/{}/stats-{}.csv"
outpath = "report/tables/{}-{}-{}.tex"

headings = ["Best", "Average", "Std Deviation", "Variance"]
realheadings = ["\\textbf{" + h + "}" for h in headings]

def writefile(file, gbest, avg):
    with open(file, "w") as f:
        line = "\\\\ \n"
        f.write("&" + " & ".join(realheadings) + line)
        f.write("\\hline " + line)
        f.write('Global Best & ' + " & ".join(gbest) + line)
        f.write('Average Fitness & ' + " & ".join(avg) + line)
        f.write("\\hline " + line)



for p in problems:

    with open(normal_path.format(p, stats[0]), "r") as file:
        line = file.readlines()[-1].split(",")
        del line[1:3]
        normgbest = [str(round(float(n), 3)) for n in line]

    with open(normal_path.format(p, stats[1]), "r") as file:
        line = file.readlines()[-1].split(",")
        del line[1:3]
        normavg = [str(round(float(n), 3)) for n in line]

    writefile(outpath.format(p, "all", ""), normgbest, normavg)
    for g, s in guide_strat:
        with open(path.format(p, g, s, stats[0]), "r") as file:
            line = file.readlines()[-1].split(",")
            del line[1:3]
            gbest = [str(round(float(n), 3)) for n in line]

        with open(path.format(p, g, s, stats[1]), "r") as file:
            line = file.readlines()[-1].split(",")
            del line[1:3]
            avg = [str(round(float(n), 3)) for n in line]

        writefile(outpath.format(p, g, s), gbest, avg)
