
class Max:

    @staticmethod
    def compare(val1, val2):
        return val2 is None or val1 >= val2

    @staticmethod
    def apply(*args):
        return max(*args)




class Min:
    @staticmethod
    def compare(val1, val2):
        return val2 is None or val1 <= val2

    @staticmethod
    def apply(*args):
        return min(*args)


