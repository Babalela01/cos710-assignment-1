from abc import abstractmethod
from random import random


class SocialGuideAlpha:
    def __init__(self, dimensions):
        self.dimensions = dimensions

    @abstractmethod
    def choose(self):
        pass

    def update(self, a):
        return a

    def postfix(self):
        return ""


class Constant(SocialGuideAlpha):
    def __init__(self, dimensions, c):
        super().__init__(dimensions)
        self.c = c

    def choose(self):
        return [self.c] * self.dimensions

    def postfix(self):
        return str(round(self.c, 2))


class Reduce(SocialGuideAlpha):
    def __init__(self, dimensions, iterations):
        super().__init__(dimensions)
        self.dec = 1 / iterations

    def choose(self):
        return [1] * self.dimensions

    def update(self, a):
        return [a[0] - self.dec] * self.dimensions


class RanDimensions(SocialGuideAlpha):
    def choose(self):
        return [random() for i in range(self.dimensions)]


class RanIteration(SocialGuideAlpha):
    def choose(self):
        return [random()] * self.dimensions

    def update(self, a):
        return [random()] * self.dimensions

