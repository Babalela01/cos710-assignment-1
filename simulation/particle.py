import random


class Particle:
    def __init__(self, i, dimensions, boundary):
        self.id = i
        self.position = [random.uniform(*boundary) for i in range(dimensions)]
        self.velocity = [0] * dimensions
        self.dimensions = dimensions
        self.boundary = boundary
        self.fitness = None
        self.best_fitness = None
        self.best_position = None
        self.boundary = boundary

    def is_feasible(self):
        # return reduce(lambda a, v: a and v in self.boundary, self.position)
        feasible, i = True, 0
        while i < self.dimensions and feasible:
            feasible = feasible and (self.boundary[0] <= self.position[i] <= self.boundary[1])
            i += 1
        return feasible

    def update_best(self, comparator):
        if comparator.compare(self.fitness, self.best_fitness) and self.is_feasible():
            self.best_position = self.position
            self.best_fitness = self.fitness

    def __lt__(self, other):
        return self.best_position < other.best_position

    def __gt__(self, other):
        return self.best_position > other.best_position

    def __eq__(self, other):
        return self.best_position == other.best_position
