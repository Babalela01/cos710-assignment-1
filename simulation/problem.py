import math
from abc import abstractmethod
from functools import reduce


# t - total, x- value
def sum_squares(_t, _x):
    return _t + _x * _x


class Problem:
    def __init__(self, boundary):
        self.boundary = boundary

    @abstractmethod
    def __call__(self, particle):
        pass


# Uni-modal [-5.12, 5.12]
class Spherical(Problem):
    def __init__(self, boundary=(-5.12, 5.12)):
        super().__init__(boundary)

    def __call__(self, particle):
        return reduce(sum_squares, particle.position, 0)


# Uni-modal [-100, 100]
class Absolute(Problem):
    def __init__(self, boundary=(-100, 100)):
        super().__init__(boundary)

    def __call__(self, particle):
        return reduce(lambda a, v: a + abs(v), particle.position, 0)


# Multi modal [-32.768, 32.768]
class Ackley(Problem):
    def __init__(self, boundary=(-32.768, 32.768)):
        super().__init__(boundary)

    def __call__(self, particle):
        lx = particle.position
        a = 20.0
        b = 0.2
        c = 2.0 * math.pi
        d = len(lx)

        sum_square = reduce(sum_squares, lx, 0)
        sum_cos = reduce(lambda t, x: t + math.cos(c * x), lx, 0)

        term1 = (-a * math.exp(-b * math.sqrt(sum_square / d)))
        term2 = -math.exp(sum_cos / d)
        return a + math.exp(1) + term1 + term2


# Multi modal [-100, 100]
class Salomon(Problem):
    def __call__(self, particle):
        lx = particle.position

        sum_square = reduce(sum_squares, lx, 0)

        sqrt = math.sqrt(sum_square)
        term1 = -math.cos(2 * math.pi * sqrt)
        term2 = 0.1 * sqrt
        return term1 + term2 + 1

    def __init__(self, boundary=(-100, 100)):
        super().__init__(boundary)


# Multi modal [-600, 600]
class Griewank(Problem):
    def __call__(self, particle):
        lx = particle.position
        sumsq = reduce(sum_squares, lx, 0)
        prod = 1

        for i in range(len(lx)):
            x = lx[i]
            prod *= math.cos(x / math.sqrt(i + 1))

        return 1 + sumsq / 4000.0 - prod

    def __init__(self, boundary=(-600, 600)):
        super().__init__(boundary)


# Multi modal [-5.12, 5.12]
class Rastrigin(Problem):
    def __call__(self, particle):
        lx = particle.position
        tmp = 0
        for x in lx:
            tmp += x * x - 10.0 * math.cos(2.0 * math.pi * x)
        return 10 * len(lx) + tmp

    def __init__(self, boundary=(-5.12, 5.12)):
        super().__init__(boundary)
