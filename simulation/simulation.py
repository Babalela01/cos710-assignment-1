import itertools
import pickle

import compare
from guidealpha import *
from problem import *
from swarm import Swarm

from velocity import *


def frange(start, stop, step):
    i = start
    while i <= stop:
        yield i
        i += step


def ran(count):
    i = 0
    while i <= count:
        yield random()
        i += 1


simCounter = itertools.count(1)

comparison = compare.Min
dimensions = 10
swarm_size = 30
iterations = 1000
runs = 50


def operation(_p, _v):
    c = next(simCounter)
    print("Simulation #{}: {} {} {}{}".format(c, type(_p).__name__, type(_v.social).__name__,
                                              type(_v.alpha_func).__name__, _v.alpha_func.postfix()))
    data = Swarm(_v, _p, dimensions, comparison, swarm_size, iterations)(runs)
    print("Simulation #{} Dumping Results".format(c))
    pickle.dump(data, open("out/{}.pickle".format(data["name"]), "wb"))
    print("Simulation #{} Complete".format(c))

#alpha_funcs = [Constant(dimensions, c) for c in (frange(0.0, 1.0, 0.1))] + \
#              [Reduce(dimensions, iterations), RanDimensions(dimensions), RanIteration(dimensions)]
alpha_funcs = [Constant(dimensions, 1)]
#social = [SocialSwarmBest(), SocialRandom()]
social = [NormalSocial()]

velocity = [Velocity(a, s) for (s, a) in itertools.product(social, alpha_funcs)]
problems = [Spherical(),
    Ackley(), Absolute(),
    Salomon(),
    Griewank(), Rastrigin()
    ]


# simulation_groups = [itertools.product(problems, [v]) for v in velocity]
simulations = itertools.product(problems, velocity)
# for p, v in simulations:
#    operation(p, v)


def single_op(pv):
    operation(*pv)


def group_op(group):
    for (p, v) in group:
        operation(p, v)
    return True

if __name__ == '__main__':
    from multiprocessing.pool import Pool
    from os import cpu_count
    workers = cpu_count() or 1
    print("{} workers\n".format(workers))
    pool = Pool(processes=workers)
    # result = pool.map(group_op, simulation_groups)
    result = pool.map(single_op, simulations)

