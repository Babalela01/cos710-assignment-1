import statistics
from operator import add
import compare
from particle import Particle


def type_name(value):
    return type(value).__name__


class Swarm:
    def __init__(swarm, velocity_func, fitness_func,
                 dimensions=10, comparator=compare.Min,
                 swarm_size=30, max_iterations=1000):
        swarm.particles = []
        swarm.iteration_index = 0
        swarm.best_position = []
        swarm.best_fitness = None

        swarm.boundary = fitness_func.boundary
        swarm.max_iterations = max_iterations
        swarm.velocity_func = velocity_func
        swarm.dimensions = dimensions
        swarm.fitness_func = fitness_func
        swarm.comparator = comparator
        swarm.swarm_size = swarm_size

    def __call__(swarm, runs=50):
        velocity_settings = swarm.velocity_func.get_settings()
        problem_name = type_name(swarm.fitness_func)
        name_postfix = swarm.velocity_func.alpha_func.postfix()
        return {
            "boundary": swarm.boundary,
            "dimensions": swarm.dimensions,
            "max-iterations": swarm.max_iterations,
            "problem": problem_name,
            "velocity": velocity_settings,
            "comparator": type_name(swarm.comparator),
            "swarm-size": swarm.swarm_size,
            "run-count": runs,
            "name": "{0}-{1}-{2}{3}".format(problem_name,
                                            velocity_settings["social"],
                                            velocity_settings["alpha"],
                                            name_postfix),
            "run-data": [swarm.run() for i in range(runs)]
        }

    def run(swarm):
        swarm.particles = [Particle(i, swarm.dimensions, swarm.boundary) for i in range(swarm.swarm_size)]
        swarm.velocity_func.start(swarm.boundary, swarm.dimensions)
        swarm.best_position = []
        swarm.best_fitness = None
        return [swarm.iteration() for swarm.iteration_index in range(0, swarm.max_iterations)]

    def iteration(swarm):
        for p in swarm.particles:
            p.fitness = swarm.fitness_func(p)
            p.update_best(swarm.comparator)
            if swarm.comparator.compare(p.best_fitness, swarm.best_fitness):
                swarm.best_position = p.best_position
                swarm.best_fitness = p.best_fitness
        for p in swarm.particles:
            p.velocity = swarm.velocity_func(p, swarm)
            p.position = list(map(add, p.position, p.velocity))
        swarm.velocity_func.iterate()
        return {"gbest": swarm.best_fitness,
                "avg_pbest": swarm.average_best(),
                "avg_fitness": swarm.average_fitness(),
                "swarm_best": swarm.current_best().fitness,
                "all_best": swarm.all_best(),
                "all_fitness": swarm.all_fitness()}

    def average_best(self):
        return statistics.mean([v.best_fitness for v in self.particles])

    def average_fitness(self):
        return statistics.mean([v.fitness for v in self.particles])

    def all_best(self):
        return [v.best_fitness for v in self.particles]

    def all_fitness(self):
        return [v.fitness for v in self.particles]

    def current_best(self):
        return self.comparator.apply(self.particles)
