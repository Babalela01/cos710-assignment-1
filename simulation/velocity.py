from random import uniform, random


def type_name(value):
    return type(value).__name__


class Cognitive:
    def __call__(self, particle):
        return particle.best_position


class NormalSocial:
    def __call__(self, swarm, alpha):
        return swarm.best_position

    def set_boundaries(self, boundary, dimensions):
        pass


class SocialSwarmBest(NormalSocial):
    def __call__(self, swarm, alpha):
        # a * x* + (1-a)y
        la = alpha
        lx = swarm.current_best().position
        ly = swarm.best_position
        return [a * x + (1 - a) * y for (a, x, y) in zip(la, lx, ly)]


class SocialRandom(NormalSocial):
    def __init__(self):
        super().__init__()
        self.x = None

    def __call__(self, swarm, alpha):
        # a * x^ + (1-a)y^
        la = alpha
        lx = self.x
        ly = swarm.best_position
        return [a * x + (1 - a) * y for (a, x, y) in zip(la, lx, ly)]

    def set_boundaries(self, boundary, dimensions):
        self.x = [uniform(*boundary) for i in range(dimensions)]


class Velocity:
    def __init__(self, alpha_func, social=NormalSocial(), cognitive=Cognitive(), w=0.789244, c1=1.4296180, c2=1.4296180):
        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.cognitive = cognitive
        self.social = social
        self.alpha_func = alpha_func

        self.a = None
        self.r1 = None
        self.r2 = None

    def __call__(self, p, swarm):
        lx = p.position
        lv = p.velocity
        cog = self.cognitive(p)
        soc = self.social(swarm, self.a)
        return [(self.w * lv[i] +
                 self.c1 * self.r1[i] * (cog[i] - lx[i]) +
                 self.c2 * self.r2[i] * (soc[i] - lx[i]))
                for i in range(swarm.dimensions)]

    def start(self, boundary, dimensions):
        self.a = self.alpha_func.choose()
        self.social.set_boundaries(boundary, dimensions)
        self.r1 = [random() for i in range(dimensions)]
        self.r2 = [random() for i in range(dimensions)]

    def iterate(self):
        self.a = self.alpha_func.update(self.a)

    def get_settings(self):
        return {"social": type_name(self.social),
                "alpha": type_name(self.alpha_func) + " " + self.alpha_func.postfix(),
                "cognitive": type_name(self.cognitive),
                "w": self.w,
                "c1": self.c1,
                "c2": self.c2
                }
